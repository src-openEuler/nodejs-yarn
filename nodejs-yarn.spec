%{?nodejs_find_provides_and_requires}

%global packagename yarn

Name:		nodejs-yarn
Version:	1.22.19
Release:	2
Summary:	Fast, reliable, and secure dependency management for yarn packages.
License:	BSD-2-Clause
URL:		https://github.com/yarnpkg/yarn
Source0:	https://registry.npmjs.org/yarn/-/yarn-1.22.19.tgz

BuildArch:	noarch

BuildRequires:	nodejs-packaging

%description
Fast, reliable, and secure dependency management for yarn packages.

%define debug_package %{nil}

%prep
%autosetup -n yarn-v%{version}
# setup the tests

%build
# nothing to do!

%install
if [ -f license ]; then
 mv license LICENSE
fi
if [ -f License ]; then
 mv License LICENSE
fi
if [ -d bin ]; then
	mkdir -p %{buildroot}%{_prefix}/local/bin/
	cp -ar bin/* %{buildroot}%{_prefix}/local/bin/
fi
mkdir -p %{buildroot}%{nodejs_sitelib}/%{packagename}
cp -ra * %{buildroot}%{nodejs_sitelib}/%{packagename}
for file in `ls ./bin`;do
	ln -sfr %{buildroot}%{nodejs_sitelib}/%{packagename}/bin/yarn.js %{buildroot}%{_prefix}/local/bin/$file
done
pushd %{buildroot}
touch filelist.lst
find . -type f -printf "/%h/%f\n" >> filelist.lst
popd
cp %{buildroot}/filelist.lst .

%check
%nodejs_symlink_deps --check

%files  -f filelist.lst
%{_prefix}/local/bin/*

%changelog
* Thu Mar 02 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.22.19-2
- Change yarn command path

* Sun Oct 09 2022 wangjunqi <wangjunqi@kylinos.cn> - 1.22.19-1
- update version to 1.22.19

* Wed Sep 15 2021 chemingdao <chemingdao@huawei.com> - 1.22.11-2
- support running command with yarn.js, yarn.cmd, yarnpkg.cmd 

* Thu Sep 09 2021 Nodejs_Bot <Nodejs_Bot@openeuler.org> - 1.22.11-1
- Package Spec generated
